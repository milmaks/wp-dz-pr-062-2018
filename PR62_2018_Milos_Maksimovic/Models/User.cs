﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace PR62_2018_Milos_Maksimovic.Models
{
    [Serializable]
    public class User
    {
        public User(string username, string password, string firstName, string lastName, Gender gender, string email, DateTime birthDate, bool active, UserType type)
        {
            Username = username;
            Password = password;
            FirstName = firstName;
            LastName = lastName;
            Gender = gender;
            Email = email;
            BirthDate = birthDate;
            Active = active;
            Role = type;
        }

        public User()
        {
            Username = string.Empty;
            Password = string.Empty;
            FirstName = string.Empty;
            LastName = string.Empty;
            Email = string.Empty;
            BirthDate = new DateTime();
            Active = false;
            Role = UserType.GUEST;
        }

        public string Username { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Gender Gender { get; set; }
        public string Email { get; set; }

        [XmlIgnore]
        public DateTime BirthDate { get; set; }
        [XmlElement("BirthDate")]
        public string SomeDateString
        {
            get { return this.BirthDate.ToString("dd/MM/yyyy"); }
            set { this.BirthDate = DateTime.Parse(value); }
        }
        public UserType Role { get; set; }
        public bool Active { get; set; }

    }
}