﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR62_2018_Milos_Maksimovic.Models
{
    public class Gallery
    {
        public Gallery(string name, string address)
        {
            Name = name;
            Address = address;
            Paintings = new List<Painting>();
            Active = true;
        }

        public Gallery()
        {
            Name = string.Empty;
            Address = string.Empty;
            Paintings = new List<Painting>();
            Active = false;
        }

        public string Name { get; set; }
        public string Address { get; set; }
        public List<Painting> Paintings { get; set; }
        public bool Active { get; set; }
    }
}