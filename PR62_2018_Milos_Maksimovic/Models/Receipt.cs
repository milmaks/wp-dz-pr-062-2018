﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace PR62_2018_Milos_Maksimovic.Models
{
    [Serializable]
    public class Receipt
    {
        public User Buyer { get; set; }
        public List<Painting> ChosenPaintings { get; set; }
        [XmlIgnore]
        public DateTime Date { get; set; }
        [XmlElement("Date")]
        public string SomeDateString
        {
            get { return this.Date.ToString("dd/MM/yyyy HH:mm:ss"); }
            set
            {
                /*
                string temp = value;
                int temp2 = 0;
                Int32.TryParse(temp[12].ToString(), out temp2);
                if(temp[11].Equals("0") && temp2 < 3)
                {
                    temp = temp + " AM";
                }
                else if(temp[11].Equals("0") && temp2 >= 3)
                {

                }
                */
                this.Date = DateTime.ParseExact(value, "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            }
        }
        public int PaidAmount { get; set; }

        public Receipt()
        {
            Buyer = new User();
            ChosenPaintings = new List<Painting>();
            Date = new DateTime();
            PaidAmount = 0;
        }

        public Receipt(User buyer, List<Painting> chosenPaintings, DateTime date, int paidAmount)
        {
            Buyer = buyer;
            ChosenPaintings = chosenPaintings;
            Date = date;
            PaidAmount = paidAmount;
        }
    }
}