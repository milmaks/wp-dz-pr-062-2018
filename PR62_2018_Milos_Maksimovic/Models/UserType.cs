﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR62_2018_Milos_Maksimovic.Models
{
    public enum UserType
    {
        ADMINISTRATOR,
        BUYER,
        GUEST
    }
}