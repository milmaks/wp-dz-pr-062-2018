﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR62_2018_Milos_Maksimovic.Models
{
    [Serializable]
    public class Painting
    {
        public Painting(string name, string author, string year, string technique, string description, int price, Status onSale)
        {
            Name = name;
            Author = author;
            Year = year;
            Technique = technique;
            Description = description;
            Price = price;
            OnSale = onSale;
            Active = true;
        }

        public Painting()
        {
            Name = string.Empty;
            Author = string.Empty;
            Year = string.Empty;
            Technique = string.Empty;
            Description = string.Empty;
            Price = 0;
        }

        public string Name { get; set; }
        public string Author { get; set; }
        public string Year { get; set; }
        public string Technique { get; set; }
        public string Description { get; set; }
        public int Price { get; set; }
        public Status OnSale { get; set; }
        public bool Active { get; set; }

        public override bool Equals(object obj)
        {
            return ((Painting)obj).GetHashCode() == this.GetHashCode();
        }

        public override int GetHashCode()
        {
            var hashCode = 41275342;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Name);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Author);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Year);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Technique);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Description);
            hashCode = hashCode * -1521134295 + Price.GetHashCode();
            hashCode = hashCode * -1521134295 + OnSale.GetHashCode();
            hashCode = hashCode * -1521134295 + Active.GetHashCode();
            return hashCode;
        }
    }
}