﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Xml;
using System.Xml.Serialization;

namespace PR62_2018_Milos_Maksimovic.Models
{
    public class DB
    {
        public static void Save<T>(List<T> elements)
        {
            if (elements == null) { return; }

            string path = string.Empty;

            if (typeof(T) == typeof(User))
            {
                path = HostingEnvironment.MapPath("~/App_Data/users.txt");
            }
            else if (typeof(T) == typeof(Gallery))
            {
                path = HostingEnvironment.MapPath("~/App_Data/galleries.txt");
            }
            else if (typeof(T) == typeof(Receipt))
            {
                path = HostingEnvironment.MapPath("~/App_Data/receipts.txt");
            }
            else
            {
                return;
            }

            try
            {
                XmlSerializer serializer = new XmlSerializer(elements.GetType());
                using (StreamWriter sw = new StreamWriter(path))
                {
                    using (MemoryStream stream = new MemoryStream())
                    {
                        serializer.Serialize(stream, elements);
                        stream.Position = 0;
                        string input = "";
                        using (StreamReader reader = new StreamReader(stream))
                        {
                            input = reader.ReadToEnd();
                        }
                        sw.WriteLine(input);
                        stream.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                
            }
        }

        public static List<T> Load<T>(string path)
        {
            List<T> ret = new List<T>();
            path = HostingEnvironment.MapPath(path);

            try
            {
                string txtDoc = "";
                using (FileStream stream = new FileStream(path, FileMode.Open))
                {
                    StreamReader sr = new StreamReader(stream);
                    string line = "";
                    while ((line = sr.ReadLine()) != null)
                    {
                        txtDoc += line.Trim();
                    }

                    using (StringReader read = new StringReader(txtDoc))
                    {
                        Type outType = typeof(List<T>);

                        XmlSerializer serializer = new XmlSerializer(outType);
                        using (XmlReader reader = new XmlTextReader(read))
                        {
                            ret = (List<T>)serializer.Deserialize(reader);
                            reader.Close();
                        }

                        read.Close();
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return ret;
        }

        public static List<Painting> UpdatePaintings(List<Gallery> galleries)
        {
            List<Painting> ret = new List<Painting>();

            foreach (Gallery gallery in galleries)
            {
                foreach (Painting painting in gallery.Paintings)
                {
                    ret.Add(painting);
                }
            }

            return ret;
        }

        public static List<Gallery> UpdatePainting(Painting painting, Painting updatedPainting)
        {
            List<Gallery> galleries = DB.Load<Gallery>("~/App_Data/galleries.txt");

            foreach (Gallery gallery in galleries)
            {
                foreach (Painting temp in gallery.Paintings)
                {
                    if(temp.Equals(painting))
                    {
                        (galleries[galleries.IndexOf(gallery)]).Paintings[gallery.Paintings.IndexOf(painting)] = updatedPainting;
                        break;
                    }
                }
            }

            DB.Save<Gallery>(galleries);
            return galleries;
        }

        

    }
}