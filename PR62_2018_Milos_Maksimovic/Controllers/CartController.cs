﻿using PR62_2018_Milos_Maksimovic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PR62_2018_Milos_Maksimovic.Controllers
{
    public class CartController : Controller
    {
        // GET: Cart
        public ActionResult Index()
        {
            List<Painting> cart = Session["cart"] as List<Painting>;
            if (cart == null)
                return RedirectToAction("Index", "Authentication");

            if (cart.Count > 0)
            {
                int total = 0;
                foreach (var item in cart)
                {
                    total += item.Price;
                }
                ViewBag.Total = total;
                ViewBag.Message = string.Empty;
            }
            else
            {
                ViewBag.Message = "Cart is empty";
            }

            return View(cart);
        }

        public ActionResult PurchaseHistory()
        {
            User user = Session["user"] as User;

            if(user == null || user.Role != UserType.BUYER)
            {
                return RedirectToAction("Index", "Authentication");
            }

            List<Receipt> receipts = HttpContext.Application["receipts"] as List<Receipt>;
            List<Receipt> buyersReceipts = new List<Receipt>();

            foreach (Receipt item in receipts)
            {
                if(item.Buyer.Username.Equals(user.Username))
                {
                    buyersReceipts.Add(item);
                }
            }

            ViewBag.Count = buyersReceipts.Count;
            return View(buyersReceipts);
        }

        public ActionResult CheckGalleries()
        {
            User user = Session["user"] as User;

            if (user == null || user.Role != UserType.BUYER)
            {
                return RedirectToAction("Index", "Authentication");
            }

            List<Gallery> galleries = HttpContext.Application["galleries"] as List<Gallery>;
            ViewBag.Show = TempData["show"];
            return View(galleries);
        }

        public ActionResult BackToMainPanel()
        {
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult Add(string index)
        {
            List<Painting> cart = Session["cart"] as List<Painting>;
            List<Painting> paintings = HttpContext.Application["paintings"] as List<Painting>;

            int temp = 0;
            Int32.TryParse(index, out temp);

            if(!cart.Contains(paintings[temp]))
            {
                cart.Add(paintings[temp]);
            }
            
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Buy()
        {
            List<Painting> cart = Session["cart"] as List<Painting>;
            List<Gallery> galleries = HttpContext.Application["galleries"] as List<Gallery>;
            List<Painting> paintings = HttpContext.Application["paintings"] as List<Painting>;

            if (cart == null)
                return RedirectToAction("Index", "Authentication");

            int total = 0;
            if (cart.Count > 0)
            {
                
                foreach (Painting item in cart)
                {
                    foreach (Gallery gallery in galleries)
                    {
                        foreach (Painting temp in gallery.Paintings)
                        {
                            if (temp.Equals(item))
                            {
                                (galleries[galleries.IndexOf(gallery)]).Paintings[gallery.Paintings.IndexOf(item)].OnSale = Status.SOLD;
                                break;
                            }
                        }
                    }
                    item.OnSale = Status.SOLD;
                    total += item.Price;
                }


                Receipt receipt = new Receipt((User)Session["user"], cart, DateTime.Now, total);
                List<Receipt> receipts = HttpContext.Application["receipts"] as List<Receipt>;

                receipts.Add(receipt);
                DB.Save<Receipt>(receipts);

                Session["cart"] = new List<Painting>();
                HttpContext.Application["paintings"] = paintings;
                HttpContext.Application["galleries"] = galleries;
                DB.Save<Gallery>(galleries);

                return RedirectToAction("PurchaseHistory");
            }

            return View("Index");
        }

        [HttpPost]
        public ActionResult RemoveAll()
        {
            Session["cart"] = new List<Painting>();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Remove(string index)
        {
            List<Painting> cart = Session["cart"] as List<Painting>;
            int temp = 0;
            Int32.TryParse(index, out temp);

            cart.RemoveAt(temp);
            Session["cart"] = cart;

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult ShowGallery(string galleryIndex)
        {
            int temp = 0;
            if(Int32.TryParse(galleryIndex, out temp))
                TempData["show"] = temp;

            return RedirectToAction("CheckGalleries");
        }
    }
}