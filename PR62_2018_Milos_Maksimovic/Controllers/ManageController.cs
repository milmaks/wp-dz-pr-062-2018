﻿using PR62_2018_Milos_Maksimovic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PR62_2018_Milos_Maksimovic.Controllers
{
    public class ManageController : Controller
    {
        // GET: Manage
        public ActionResult Index()
        {
            User user = Session["user"] as User;

            if(user != null)
            {
                if(user.Role == UserType.ADMINISTRATOR)
                {
                    List<User> users = HttpContext.Application["users"] as List<User>;
                    return View(users);
                }
            }

            return RedirectToAction("Index", "Authentication");
        }

        public ActionResult Galleries()
        {
            User user = Session["user"] as User;

            if (user != null)
            {
                if (user.Role == UserType.ADMINISTRATOR)
                {
                    List<Gallery> galleries = HttpContext.Application["galleries"] as List<Gallery>;
                    return View(galleries);
                }
            }

            return RedirectToAction("Index", "Authentication");
        }

        public ActionResult BackToMainPanel()
        {
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult DeleteUser(string username)
        {
            List<User> users = HttpContext.Application["users"] as List<User>;
            int index = 0;

            foreach (User user in users)
            {
                if (user.Username.Equals(username))
                {
                    index = users.IndexOf(user);
                    break;
                }
            }

            users[index].Active = false;
            DB.Save<User>(users);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult AddGallery()
        {
            TempData["Add"] = true;
            TempData["Message"] = "*Required";
            return RedirectToAction("Galleries");
        }

        [HttpPost]
        public ActionResult DeleteGallery(string galleryIndex)
        {
            List<Gallery> galleries = HttpContext.Application["galleries"] as List<Gallery>;
            
            int index = 0, i = 0;
            Int32.TryParse(galleryIndex, out index);

            foreach (Painting painting in galleries[index].Paintings)
            {
                galleries[index].Paintings[i++].Active = false;
            }

            galleries[index].Active = false;
            DB.Save<Gallery>(galleries);
            

            return RedirectToAction("Galleries");
        }

        [HttpPost]
        public ActionResult NewGallery(string name, string address)
        {
            if(name.Length < 1)
            {
                TempData["Message"] = "Name must be at least 1 character long";
                TempData["Add"] = true;
                return RedirectToAction("Galleries");
            }
            string[] adr;
            adr = address.Split(',');
            if(adr.Length != 3)
            {
                TempData["Message"] = "Correct format: Street, Number, City";
                TempData["Add"] = true;
                return RedirectToAction("Galleries");
            }
            else
            {
                if((adr[0].Trim()).Length < 1)
                {
                    TempData["Message"] = "Street missing, correct format: Street, Number, City";
                    TempData["Add"] = true;
                    return RedirectToAction("Galleries");
                }
                if(!Int32.TryParse(adr[1], out _))
                {
                    TempData["Message"] = "Invalid number, correct format: Street, Number, City";
                    TempData["Add"] = true;
                    return RedirectToAction("Galleries");
                }
                if ((adr[2].Trim()).Length < 1)
                {
                    TempData["Message"] = "City missing, correct format: Street, Number, City";
                    TempData["Add"] = true;
                    return RedirectToAction("Galleries");
                }
            }
            List<Gallery> galleries = HttpContext.Application["galleries"] as List<Gallery>;
            Gallery gallery = new Gallery(name, address);
            galleries.Add(gallery);
            DB.Save<Gallery>(galleries);

            TempData["Add"] = false;
            return RedirectToAction("Galleries");
        }
    }
}