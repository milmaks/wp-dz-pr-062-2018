﻿using PR62_2018_Milos_Maksimovic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace PR62_2018_Milos_Maksimovic.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            User user = Session["user"] as User;

            if (user == null || user.Username.Equals(string.Empty) && user.Role != UserType.GUEST)
            {
                return RedirectToAction("Index", "Authentication");
            }

            if(user.Role == UserType.ADMINISTRATOR)
            {
                ViewBag.Guest = false;
                ViewBag.User = user;
            }
            else if(user.Role == UserType.BUYER)
            {
                List<Painting> cart = Session["cart"] as List<Painting>;
                if(cart == null)
                {
                    Session["cart"] = new List<Painting>();
                }
                ViewBag.Guest = false;
                ViewBag.User = user;
                HttpContext.Application["receipts"] = DB.Load<Receipt>("~/App_Data/receipts.txt");
            }
            else
            {
                ViewBag.Guest = true;
            }

            List<Painting> paintings = HttpContext.Application["paintings"] as List<Painting>;

            return View(paintings);
        }

        public ActionResult AddPainting()
        {
            User user = Session["user"] as User;

            if (user == null || user.Username.Equals(string.Empty) && user.Role != UserType.ADMINISTRATOR)
            {
                return RedirectToAction("Index", "Authentication");
            }

            ViewBag.Galleries = HttpContext.Application["galleries"] as List<Gallery>;
            ViewBag.Message = "*Required";
            Painting painting = new Painting();

            return View(painting);
        }

        public ActionResult ModifyPainting()
        {
            User user = Session["user"] as User;

            if (user == null || user.Username.Equals(string.Empty) && user.Role != UserType.ADMINISTRATOR)
            {
                return RedirectToAction("Index", "Authentication");
            }

            ViewBag.Message = TempData["Message"] == null ? "*Required" : TempData["Message"];
            Painting painting = TempData["paintingToModify"] as Painting;
            return View(painting); 
        }

        [HttpPost]
        public ActionResult ModifyPainting(int position)
        {
            List<Painting> paintings = HttpContext.Application["paintings"] as List<Painting>;

            TempData["paintingToModify"] = paintings[position];
            TempData["position"] = position;

            return RedirectToAction("ModifyPainting");
        }

        [HttpPost]
        public ActionResult Modify(Painting painting)
        {
            if (painting.Name == null || painting.Author == null || painting.Technique == null || painting.Year == null)
            {
                TempData["Message"] = "All required fields must be filled";
                TempData["paintingToModify"] = painting;
                return RedirectToAction("ModifyPainting");
            }
            if (painting.Name.Length < 1)
            {
                TempData["Message"] = "Name must be at least 1 character long";
                TempData["paintingToModify"] = painting;
                return RedirectToAction("ModifyPainting");
            }
            if ((painting.Author.Split(' ')).Length < 2)
            {
                TempData["Message"] = "Input authors name and lastname";
                TempData["paintingToModify"] = painting;
                return RedirectToAction("ModifyPainting");
            }
            if (!Int32.TryParse(painting.Year, out _))
            {
                TempData["Message"] = "Invalid year";
                TempData["paintingToModify"] = painting;
                return RedirectToAction("ModifyPainting");
            }
            if (painting.Technique.Length < 1)
            {
                TempData["Message"] = "Technique required";
                TempData["paintingToModify"] = painting;
                return RedirectToAction("ModifyPainting");
            }

            List<Painting> paintings = HttpContext.Application["paintings"] as List<Painting>;
            int temp = 0;
            Int32.TryParse(TempData["position"].ToString(), out temp);

            painting.Active = true;
            HttpContext.Application["galleries"] = DB.UpdatePainting(paintings[temp], painting);
            paintings[temp] = painting;
            HttpContext.Application["paintings"] = paintings;

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult NewPainting(Painting p, string galleryIndex)
        {
            if(p.Name == null || p.Author == null || p.Technique == null || p.Year == null)
            {
                ViewBag.Message = "All required fields must be filled";
                ViewBag.Galleries = HttpContext.Application["galleries"] as List<Gallery>;
                return View("AddPainting");
            }
            if (p.Name.Length < 1)
            {
                ViewBag.Message = "Name must be at least 1 character long";
                ViewBag.Galleries = HttpContext.Application["galleries"] as List<Gallery>;
                return View("AddPainting");
            }
            if ((p.Author.Split(' ')).Length < 2)
            {
                ViewBag.Message = "Input authors name and lastname";
                ViewBag.Galleries = HttpContext.Application["galleries"] as List<Gallery>;
                return View("AddPainting");
            }
            if (!Int32.TryParse(p.Year, out _))
            {
                ViewBag.Message = "Invalid year";
                ViewBag.Galleries = HttpContext.Application["galleries"] as List<Gallery>;
                return View("AddPainting");
            }
            if (p.Technique.Length < 1)
            {
                ViewBag.Message = "Technique required";
                ViewBag.Galleries = HttpContext.Application["galleries"] as List<Gallery>;
                return View("AddPainting");
            }

            if(galleryIndex == null)
            {
                ViewBag.Message = "Gallery required";
                ViewBag.Galleries = HttpContext.Application["galleries"] as List<Gallery>;
                return View("AddPainting");
            }

            List<Gallery> galleries = HttpContext.Application["galleries"] as List<Gallery>;
            List<Painting> paintings = HttpContext.Application["paintings"] as List<Painting>;

            int temp = 0;
            Int32.TryParse(galleryIndex, out temp);

            p.Active = true;
            paintings.Add(p);
            galleries[temp].Paintings.Add(p);

            //DB.Save<Painting>(paintings);
            DB.Save<Gallery>(galleries);

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult LogOut()
        {
            return RedirectToAction("LogOut", "Authentication");
        }

        [HttpPost]
        public ActionResult ManageUsers()
        {
            return RedirectToAction("Index", "Manage");
        }

        [HttpPost]
        public ActionResult ManageGalleries()
        {
            return RedirectToAction("Galleries", "Manage");
        }

        [HttpPost]
        public ActionResult Add()
        {
            return RedirectToAction("AddPainting");
        }

        [HttpPost]
        public ActionResult Cart()
        {
            return RedirectToAction("Index", "Cart");
        }

        [HttpPost]
        public ActionResult PurchaseHistory()
        {
            return RedirectToAction("PurchaseHistory", "Cart");
        }

        [HttpPost]
        public ActionResult CheckGalleries()
        {
            return RedirectToAction("CheckGalleries", "Cart");
        }

        [HttpPost]
        public ActionResult Sort(string field, string type)
        {
            List<Painting> paintings = HttpContext.Application["paintings"] as List<Painting>;       
            List<Painting> ret = new List<Painting>();

            if (field == "name")
            {
                List<string> temp = new List<string>(paintings.Count);
                foreach (Painting painting in paintings)
                {
                    temp.Add(painting.Name);
                }

                if (type.Equals("a"))
                    temp.Sort();
                else
                    temp.Reverse();
                Painting p = new Painting();

                foreach (string item in temp)
                {
                    foreach (Painting painting in paintings)
                    {
                        if (painting.Name.Equals(item))
                        {
                            ret.Add(painting);
                            p = painting;
                        }
                    }
                    paintings.Remove(p);
                }
            }
            else if (field == "technique")
            {
                List<string> temp = new List<string>(paintings.Count);
                foreach (Painting painting in paintings)
                {
                    temp.Add(painting.Technique);
                }

                if (type.Equals("a"))
                    temp.Sort();
                else
                    temp.Reverse();
                Painting p = new Painting();

                foreach (string item in temp)
                {
                    foreach (Painting painting in paintings)
                    {
                        if (painting.Technique.Equals(item))
                        {
                            ret.Add(painting);
                            p = painting;
                        }
                    }
                    paintings.Remove(p);
                }
            }
            else
            {
                List<int> temp = new List<int>(paintings.Count);
                foreach (Painting painting in paintings)
                {
                    temp.Add(painting.Price);
                }

                if (type.Equals("a"))
                    temp.Sort();
                else
                    temp.Reverse();
                Painting p = new Painting();

                foreach (int item in temp)
                {
                    foreach (Painting painting in paintings)
                    {
                        if (painting.Price == item)
                        {
                            ret.Add(painting);
                            p = painting;
                        }
                    }
                    paintings.Remove(p);
                }
            }

            HttpContext.Application["paintings"] = ret;
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Search(string field, string searchParm)
        {
            List<Painting> paintings = HttpContext.Application["paintings"] as List<Painting>;
            List<Painting> ret = new List<Painting>();

            Regex patern = new Regex(@"." + searchParm.ToLower() + ".|." + searchParm.ToUpper() + ".");
            Regex patern2 = new Regex(@"" + searchParm.ToLower() + ".|" + searchParm.ToUpper() + ".");

            if (field == "name")
            {
                foreach (Painting painting in paintings)
                {
                    if (patern.Match(painting.Name).Success || patern2.Match(painting.Name).Success)
                        ret.Add(painting);
                }
            }
            else
            {
                foreach (Painting painting in paintings)
                {
                    if (patern.Match(painting.Technique).Success || patern2.Match(painting.Technique).Success)
                        ret.Add(painting);
                }
            }

            HttpContext.Application["paintings"] = ret;
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult SearchPrice(string searchPriceMin, string searchPriceMax)
        {
            if (!searchPriceMax.Equals(string.Empty) && !searchPriceMin.Equals(string.Empty))
            {
                List<Painting> paintings = HttpContext.Application["paintings"] as List<Painting>;
                List<Painting> ret = new List<Painting>();
                int minPrice, maxPrice;

                Int32.TryParse(searchPriceMin, out minPrice);
                Int32.TryParse(searchPriceMax, out maxPrice);

                foreach (Painting painting in paintings)
                {
                    if (painting.Price > minPrice && painting.Price < maxPrice)
                        ret.Add(painting);
                }

                HttpContext.Application["paintings"] = ret;
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Cancel()
        {
            HttpContext.Application["paintings"] = DB.UpdatePaintings(HttpContext.Application["galleries"] as List<Gallery>);
            return RedirectToAction("Index");
        }
    }
}