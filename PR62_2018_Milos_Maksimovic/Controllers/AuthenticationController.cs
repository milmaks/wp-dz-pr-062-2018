﻿using PR62_2018_Milos_Maksimovic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace PR62_2018_Milos_Maksimovic.Controllers
{
    public class AuthenticationController : Controller
    {
        // GET: Authentication
        public ActionResult Index()
        {
            ViewBag.Message = "*Required";
            return View();
        }

        public ActionResult Register()
        {
            ViewBag.Message = "*Required";
            User user = new User();
            Session["user"] = user;
            return View(user);
        }


        [HttpPost]
        public ActionResult Register(User user)
        {
            List<User> users = HttpContext.Application["users"] as List<User>;

            if (user.Username == null || user.Password == null || user.FirstName == null || user.LastName == null || user.BirthDate == new DateTime(1,1,1) || user.Email == null)
            {
                ViewBag.Message = "All required fields must be filled";
                return View("Register");
            }
            if((user.Username.Trim()).Length < 3)
            {
                ViewBag.Message = "Username must be at least 3 characters long";
                return View("Register");
            }
            if(!Regex.Match(user.Password, @"[A-Za-z0-9]{8}").Success)
            {
                ViewBag.Message = "Password must be at least 8 characters long\nonly letters and numbers";
                return View("Register");
            }
            if(!Regex.Match(user.Email, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Success)
            {
                ViewBag.Message = "Wrong email format";
                return View("Register");
            }
            if(users.Find(u => u.Username.Equals(user.Username) && u.Active) != null)
            {
                ViewBag.Message = "User with that username already exists";
                return View("Register");
            }

            user.Active = true;
            user.Role = UserType.BUYER;
            users.Add(user);
            DB.Save<User>(users);

            return View("Index");
        }

        public ActionResult GuestLogin()
        {
            User user = new User();
            Session["user"] = user;
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult Login(string username, string password)
        {
            if((username.Trim()).Equals(string.Empty) || (password.Trim()).Equals(string.Empty))
            {
                ViewBag.Message = "All required fields must be filled";
                return View("Index");
            }

            List<User> users = HttpContext.Application["users"] as List<User>;
            User user = users.Find(u => u.Username.Equals(username) && u.Password.Equals(password));

            if(user == null)
            {
                ViewBag.Message = "User with this username and password does not exist";
                return View("Index");
            }

            if(user.Active == false)
            {
                ViewBag.Message = "User with this username and password does not exist";
                return View("Index");
            }

            Session["user"] = user;

            return RedirectToAction("Index", "Home");
        }

        public ActionResult LogOut()
        {
            Session["user"] = null;
            Session["cart"] = null;

            return View("Index");
        }
    }
}