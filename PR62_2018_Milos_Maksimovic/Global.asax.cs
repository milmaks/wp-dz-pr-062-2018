﻿using PR62_2018_Milos_Maksimovic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace PR62_2018_Milos_Maksimovic
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            List<User> users = DB.Load<User>("~/App_Data/users.txt");
            HttpContext.Current.Application["users"] = users;

            List<Gallery> galleries = DB.Load<Gallery>("~/App_Data/galleries.txt");
            HttpContext.Current.Application["galleries"] = galleries;

            List<Painting> paintings = DB.UpdatePaintings(galleries);
            HttpContext.Current.Application["paintings"] = paintings;
        }
    }
}
